/*Learning Objectives: 
-create code that will execute only when a given condition
has been made.

Topics:

-Assignment Operators
-Condtitional Statements
-Ternary Operator



Conditional Statements
	- a conditional statements is one of the key features 
	of a programming language

	Types:

	if-else
	switch
	try-catch finally

	operators:
	-	 allow prog langs to exec operations or

	Assignment Operators:

	-	Assign value to a variable
	Basic Assignemt Operator (=)

	let variable ="Initial Value";



	Math Operators(+, -, *, /)
	-	 whenever you have used a math operator, a value is returned, 
	it is only up to us if we save that returned value.

	add,sub,mul,div assignment operators allows us to assieng the result
	of the operation to the value of the left operand. It allows us to
	save the results of a mathe operaion to the left operand.
*/
	let num1 = 5;
	let num2 = 10;
	let num3 = 55;
	let num4 = 52;
/*

Addition Assignment Operator(+=):
Left operand 
-	Variable or value of the left side of the operator.
Right Operand
-	Variable or value pf the right side of the operator.
ex: sum = num1 + num4; re-assgined the value of sum with the result of 
num1 + num4

*/

let sum = num1 + num4;
num1 = num1 + num4; /*is same num1+= num4;         */
console.log(num1);
num2 += num3;
console.log(num2);
num1 += 55;
console.log(num1);
console.log(num4);

let string1 = "Boston";
let string2 = 'Celtics';

string1 += string2;
console.log	(string1);
//results in concatenation.
console.log(string2);

// 15 += num1; we do not use assignment operator when the left operand is
//value or data

//Subtraction Assignment operator(-=)

num1 -= num2;
console.log(num1);

//multiplication assignment operator (*=)
num2*= num4;
console.log(num2);

//division assignment operator(/=)
num4/= num3;
console.log	(num4);

//*********************[SECTION]**********************//

//Arithmetic Operators
let x = 1397;
let y = 7831;

let sum1 = x + y;
console.log("Result of addition operators: " + sum1);

let diff = y - x;
console.log("Result of subtraction operator: " + diff);


let prod = x*y;
console.log("Result of multiplication operator: " + prod);

let quo = y/x;
console.log("Result of division operator: " + quo);

let rem = y % x;
console.log("Result of modulis operator: " + rem);
	
//Mutiple operators and parthesis

/*
	-when multiplple operators is applied it follows PEMDAS
	(parenthesis, exponent, mul, div, add, sub)

*/
let mdas = 1+ 2 - 3 * 4 /5;
console.log(mdas);

let pemdas = 1 + (2 - 3 ) * (4/5);
console.log	(pemdas);

pemdas =(1 + (2-3)) * (4/5);
console.log(pemdas);



/*
Increment and decrement
	Increment and decrement is adding or subtracting 1 from the 
	varibale and re-assigning the new value to the variable 
	where the increment or decrement was used.

	2 kinds of incrementation: pre-fix and post-fix
*/

let z = 1;

//prefix

++z;
console.log("prefix increment: "+ z);

//postfix

z++;
console.log("Postfix increment: " + z);
// value of z was added with 1

console.log(z++);// 3 - with post-incrementation the previous value 
// value of the variable is returned first before the actual incrementation

console.log(z);// 4 - newvalue is now returned.

//prefix vs post fix

console.log(z);
console.log(z++);// 4 previous value was returned first
console.log(z);// 5 new valie is now returned


console.log(++z);//6

//prefix and post fix decerement


console.log(--z);
console.log(z--);
console.log(z);


//Comparison operators
/*
comparison operators are used to compare the valies pf the 
left and right operatnds.

-comparison opertaors return boolean

	- loosley equality opertaor(==)
	- strict equality operator (===)
*/

console.log(1 == 1);

let isSame = 55 == 55;

console.log(isSame);

console.log(1 == '1'); //true - loose equality operator priority
// is the sameness of the value because with loose equality
// opertaro, forced coercion is done before comparison.

console.log(0 == false);//true - with forced coercion, false
// was converted into a number results into NaN, so therefore,
// 1 is not equal to NaN

console.log(1 == true);//true
console.log('false' == false);//false

/*
with loose comparison operator (==), values are compared 
and types if operands do not have the same types, it will
be forced coerced/type coerce before comparison value.


if either the operand is a number of bool, the operands are 
converted into numbers.

*/

console.log(true == "1"); // true - true coerced into a number = 1,
//"1" converted into a number = 1 - 1 equals 1


/*Strict equality (===)*/

console.log(true === "1");//false

//checks both value and type 

console.log(1 === '1'); // false - both operands have same value 
// but different types.

console.log('BTS' === "BTS");//true -same valiue and type

console.log('Marie' === 'marie');//false -left operand Captialized
// while right is not.



/*Inequality operators (!=)*/

/*loose inequality operators
	- checks whether the operands are not equal or 
	have different values

	different values
	-will do type coercion if the operances have different.
*/

console.log('1' != 1);// false

/*
false = both operands were tconverted to numbers 
'1' converted into number is 1

1 converted into number 1 is 1 - 1 equals 1 is not equal
*/

console.log('James' != 'John');//true

console.log(1 != true);//false


/*
false = both operands were conversion: true was converted to 1
1 is equal to 1.

*/

/*
Strict inequlaity (!==)

checks whether the two operands have differnet values and will check
if tthey have different types
*/

console.log('5'!== 5); //true - different types
console.log(5!==5); //false - same types


let name1 = 'Jin';
let name2 = 'Jimin';
let name3 = 'Jungkook';
let name4 = 'V';

let number = 50;
let number2 = 60;
let numString1 = '50';
let numString2 = '60';


console.log(numString1 == number);//true
console.log(numString1 === number);//false
console.log(numString1 != number);//false
console.log(name4 !== 'num3');//true
console.log(name3 == 'Jungkook');//true
console.log(name1 === "Jin"); //true

/*
Relational comparison operators
	a comparison operator compares its operands and returns 
	a boolean value
*/

let a = 500;
let b = 700;
let c = 8000;

let numString3 = '5500';

//greater than (>)

console.log(a>b);
console.log(c>b);

//less than (<)

console.log(c<a);//f
console.log(b<b);//f
console.log(a<1000);//t
console.log(numString3 < 1000);//false - forced coercion
//to change the string into number.

console.log(numString3 < 6000); //t
console.log(numString3 <'Jose');//t - '5500' < 'jose'- erratic (unpredictable)


/*
Greater than or equal to
*/

console.log(c >= 100000); //f
console.log(b>= a); //t

//less thna or equal to

console.log(a <= b);//t
console.log(c <= a); //f

//logical operators
/*
AND (&&)
	-both operadns on the left and the right or all operands 
	must be true otherwise false

	T && T = t
	T && F = f
	F && T = f
	F && F = f

*/

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1);//f

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); // t


let requiredLevel = 95;
let requiredAge = 18;

let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3);//f

let authorization4 = isRegistered && isLegalAge && requiredLevel 
=== 95;
console.log(authorization4);// t


let userName = 'gamer2001';
let userName2 = 'shadow1991';
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= requiredAge;

// .legnth is a property of string which determine the number of characters
// in a string.

console.log(registration1);

let registration2 = userName.length > 8 && userAge2 >= requiredAge;
console.log(registration2); //t

/*
OR oprerator (|| - double pipe)
- returns true if atleast one of the operands are true.

T || T = t
T || F = t
F || T = t
F || F = f
*/


let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel >= requiredLevel &&
userAge >= requiredAge;
console.log(guildRequirement);//f

let guildRequirement1 = isRegistered || userLevel >= requiredLevel ||
userAge >= requiredAge;
console.log(guildRequirement1);//t

let guildRequirement2 = userLevel >= requiredLevel ||
userAge >= requiredAge;
console.log(guildRequirement2);//t

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);//f

//(!)Not Operator - it turns a boolean into the opposite value 

let guildAdmin2 = !isAdmin || userLevel2 >=requiredLevel;
console.log(guildAdmin2);//t
console.log(!isRegistered);//f

// *********************[SECTION]**************************//
/*
Conditional
	
	if-else statements - will run a block of code if the condition
	specified is true or results to true.
*/


let userName3 = "crusader_3000";
let userLevel3 = 25;
let userAge3 = 20;

//if(true){
//	console.log("We just ran an if condition!");
//}

if (userName3.length > 10){
	console.log("Welcome to Game Online!");

}

if (userLevel3 >= requiredLevel){

	console.log('you are qualified t /**/o join the guild');
} else {
	console.log('Good bye');
}
/*
else statement will be run if the condition give is false */
	// else if - executes a statement if the previous or the original condition is false or resulted to false but another specified condition resulted to true
	if(userName3.length >= 10 && userLevel3 <= 25 && userAge >= requiredAge) {
		console.log("Thank you for joining the Noobies Guild");
	} else if(userLevel3 > 25){
		console.log("You are too strong to be a noob.");
	} else if(userAge3 < requiredAge) {
		console.log("You are too young to join the guild.");
	} else if(userName3.length < 10) {
		console.log("Username too short.");
	};

	// if-else in function

	/*
	typeof keyword returns a string which tells the type of data
	that follow it.
	*/
	function addNum(num1, num2){

		if (typeof num1 === 'number' && typeof num2 === 'number'){
			console.log("run only if both arguments  passed are number types.");
			console.log(num1 + num2);
		}else{
			console.log("one or both arguments are not numbers");
		}
	}

	addNum(2,'4');
// ************************** MINI ACTIVITY ************************//
/*	function login(un, pass){
		//how can we check if the argument passed are strings?

		if(typeof un === 'string' && typeof pass === 'string'){
					console.log("both arguments are strings");

		} if (un.length >= 8 && pass.length >= 8){
					console.log("Logged in");	

				} else if (!un.length > 8) {
					alert("username is too short.");
				 
				} else if (!pass.length > 8) {
					alert ('password is too short');
				} else {
					alert("Credentials are too short");
				}
			}		
	login('gaming_gamer', 'gameNation');

*/
function colour(day){


if(typeof day !=='string'){
alert("Invaid Input. Please input a string.");
}

let lowerCase = day.toLowerCase();

if ('monday' == lowerCase){
alert ("Today is " + lowerCase  + ", Wear Black");
}

else if ('tuesday' == lowerCase ){
alert ("Today is " + lowerCase  + ", Wear Green");
	
}
else if ('wednesday' == lowerCase ){
alert ("Today is " + lowerCase  + ", Wear Yellow");
	
}
else if ('thursday' == lowerCase ){
alert ("Today is " + lowerCase  + ", Wear Red");
	
}
else if ('friday' == lowerCase ){
alert ("Today is " + lowerCase  + ", Wear Violet");
	
}
else if ('saturday' == lowerCase ){
alert ("Today is " + lowerCase  + ", Wear Blue");
	
}
else if ('sunday' == lowerCase ){
alert ("Today is " + lowerCase  + ", Wear White");
	
} else {
	alert("Invalid input. Enter a valid day of the week.");
}

}
colour("mOnDAY");

/*
Switch statement
	-used as an alternative to an if else tree.

syntax:
	switch(condition){
	case value:
		statement;
		break;
	default:
		statement;
		break;
	}
example:
*/
let hero ='Anpanman';

switch(hero){
	case "Jose Rizal":
		console.log("Ph hero");
		break;

	case "George washingtong":
		console.log('us hero');
		break;
	
	case "hercules":
		console.log('mythical hero');
		break;

	case 'Anpanman':
		console.log('Anime hero');
		break;

	}

	function rolechecker(role){
		switch(role){

			case 'Admin':
				console.log	('welcome admin, to the dashboard.');
				break;

			case'user':
				console.log('you are not authorized');
				break;

			case 'guest':
				console.log('go to registration page to register.');
				break;
			default:
				console.log('bye');

		}	
	}

	rolechecker('Admin');
//break terminates

function colour_of_the_day(day){

if(typeof day !=='string'){
	alert("Invaid Input. Please input a string.");
} else {
let lowerCase1 = day.toLowerCase();

switch(day){

	case "monday":
		alert ("Today is " + lowerCase1  + ", Wear Black");
		break;


	case 'tuesday':
		alert ("Today is " + lowerCase1  + ", Wear Green");
		break;

	case 'wednesday':
		alert ("Today is " + lowerCase1  + ", Wear Yellow");
		break;

	case 'thursday':
		alert ("Today is " + lowerCase1  + ", Wear Red");
		break;


	case 'friday':
		alert ("Today is " + lowerCase1  + ", Wear Violet");
		break;

	case 'saturday':
		alert ("Today is " + lowerCase1  + ", Wear Blue");
		break;

	case 'sunday':
		alert ("Today is " + lowerCase1  + ", Wear White");
		break;

	default:
		alert("Invalid input. Enter a valid day of the week.");
		break;
};

} 
	

};

colour_of_the_day("yes");

/*
	Mini-Activity
	  create a colorOfTheDay function, instead of using if-else, convert it to a switch(use switch statement)
 */

// function with if-else and return


// Miss' version

function gradeEvaluator(grade){
	/*
		evaluate the grade input and return the letter distinction
			 - if the grade is less than or equal to 70 = F
			 - if the grade is greater than or equal to 71 = C
			 - if the grade is greater than or equal to 80 = B
			 - if the grade is greater than or equal to 90 = A
		resume: 3:45 PM	
	*/
	if(grade >= 90){
		return keyword can be used in an if-else statement inside a function
			- it allows the function to return a value
		
		return "A";
	} else if(grade >= 80){
		return "B";
	} else if(grade >= 71){
		return "C";
	} else if(grade <= 70){
		return "F"
	} else {
		return "Invalid"
	};
};

let grade = gradeEvaluator(75);
console.log(`The result is: ${grade}`);

/* Ternary Operators */
	// shorthand way of writing if-else statements
	/*
		syntax:
		condition ? if-statement : else statement
	*/

let price = 5000; 

price > 1000 ? console.log("Price is over 1000") : console.log("Price is less than 1000")

let hero1 = "Goku";

hero1 === "Vegeta" ? console.log("You are the Prince of all Saiyans.") : console.log("You are not even royalty.")	

let villain = "Harvey Dent";

villain === "Two Faces"
? console.log("You live long enough to be a villain.")
: console.log("Not quite villainous yet.")

let robin = "Dick Grayson";
let currentRobin = "Tim Drake";

let isFirstRobin = currentRobin === robin 
? true
: false;
console.log(isFirstRobin);

let numberz = 7;

//ternary operator is not meant for complex
/*number === 5
? console.log("A")
: (number === 10) console.log("A is 10") : console.log("A is not 5 or 10));


gradea = 100;
switch(gradea){
		case 100:
			console.log('F');
			break;
		default: 
			console.log('Hello')
	}

